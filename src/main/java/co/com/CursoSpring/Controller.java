package co.com.CursoSpring;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @GetMapping(value = "/")
    public String getHolaMundo(){
        return "Hola ¿como estás?";
    }
}
